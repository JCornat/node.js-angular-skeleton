(function () {
    var path = require('path');

    module.exports = function (app) {
        app.get('/api', function (req, res) {
            res.send([{name: 'Foo'}, {name: 'Bar'}]);
        });

        app.all('/*', function(req, res) {
            res.sendFile(path.join(__dirname, '../view', 'index.html'));
        });
    }
})();