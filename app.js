(function () {

    //Main Modules
    var express = require('express');
    var session = require('express-session');
    var bodyParser = require('body-parser');

    //Additional Modules
    var config = require(__dirname + '/config/config.js');

    //Launch server
    var app = express();
    var server = require('http').Server(app);

    app.use(bodyParser.json()); // for parsing application/json
    app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

    app.use(function (req, res, next) {
        console.log(new Date().toISOString()+' - '+req.url+' - '+req.method+' - '+JSON.stringify(req.params)+' - :'+JSON.stringify(req.query));
        next();
    });

    app.use('/public', express.static(__dirname + '/public'));

    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
        next();
    });

    //Session
    app.use(session({secret: config.cookie, resave: false, rolling: true, saveUninitialized: false, cookie:{maxAge: 14400000}}));

    //Routing
    require(__dirname + '/controller/route.js')(app);

    server.listen(config.port);
    console.log('Server listening on port ' + config.port);
})();
