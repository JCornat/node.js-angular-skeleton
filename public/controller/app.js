(function () {
    var app = angular.module('app', [
        'ui.router', 'ngAnimate', 'ngSanitize'
    ]);

    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    '': {
                        templateUrl: '/public/view/home.html',
                        controller: 'HomeCtrl as ctrl'
                    }
                }
            })
            .state('login', {
                url: '/login',
                views: {
                    '': {
                        templateUrl: '/public/view/login.html',
                        controller: 'LoginCtrl as ctrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('home');
        $locationProvider.html5Mode(true)
    }]);

    app.run(['$rootScope', function($rootScope) {
        $rootScope.previousState = {};
        $rootScope.currentState = {};
        $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = {name: from.name, params: fromParams};
            $rootScope.currentState = {name: to.name, params: toParams};
        });
    }]);

    console.log('LOAD');

    app.controller('HomeCtrl', ['$state', function($state) {
        var self = this;
        console.log('HOME');

        self.name = 'Coucou';

        self.go = function (route) {
            $state.go(route);
        };
    }]);

    app.controller('LoginCtrl', ['$state', function($state) {
        var self = this;

        console.log('LoginCtrl');
        self.go = function (route) {
            $state.go(route);
        };
    }]);
})();