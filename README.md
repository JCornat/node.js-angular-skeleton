# Node.js Angular Skeleton #

### How to set up ###

* Pull repository and go in root folder with a terminal
* Launch command "npm i"
* If you **don't** get **gulp** installed, launch command "npm i -g gulp"
* Launch command "bower install"
* If you **don't** get **bower** installed, launch command "npm i -g bower"
* Copy `config/config.js.default` file to `config/config.js`
* Fill `config/config.js` with port number (ie **8080**) and cookie string (ie: random string, like : 'zzqkdhluidquilzgiulzglugdqlgd')